package grails.canary

import grails.converters.JSON
import grails.util.Metadata

class StatusController {

    def index() {

        def sout = new StringBuilder()
        def serr = new StringBuilder()
        def output = 'uname -a'.execute()
        output.consumeProcessOutput(sout, serr)
        output.waitForOrKill(1000)

        def parameters = [:]
        parameters['java.version'] = System.getProperty('java.version')
        parameters['groovy.version'] = GroovySystem.version
        parameters['grails.version'] = Metadata.current.'app.grails.version'
        parameters['app.name'] = Metadata.current.'app.name'
        parameters['hostname'] = sout.toString()

        render parameters as JSON
    }
}
