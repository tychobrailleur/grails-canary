package grails.canary

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(StatusController)
class StatusControllerSpec extends Specification {

    void "index returns details about server"() {
        when:
        def output = controller.index()

        then:
        response
        response.json.'groovy.version' == '2.4.10'
        response.json.'grails.version' == '2.5.6'
        response.json.'app.name' == 'grails-canary'
    }
}
